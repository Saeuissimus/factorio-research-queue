data:extend({
    {
        type = "bool-setting",
        name = "research-queue_popup-on-queue-finish",
        localised_name = {"rq-settings.popup-on-queue-finish"},
        localised_description = {"rq-settings.popup-on-queue-finish-detailed"},
        setting_type = "runtime-per-user",
        default_value = false
    },
    {
        type = "int-setting",
        name = "research-queue-rows-count",
        localised_name = {"rq-settings.row-queue-view"},
        localised_description = {"rq-settings.row-queue-view-detailed"},
        setting_type = "runtime-per-user",
        default_value = 8
    },
    {
        type = "int-setting",
        name = "research-queue-table-width",
        localised_name = {"rq-settings.table-width"},
        localised_description = {"rq-settings.table-width-detailed"},
        setting_type = "runtime-per-user",
        default_value = 10
    },
    {
        type = "int-setting",
        name = "research-queue-table-height",
        localised_name = {"rq-settings.table-height"},
        localised_description = {"rq-settings.table-height-detailed"},
        setting_type = "runtime-per-user",
        default_value = 8
    },
    {
        type = "int-setting",
        name = "research-queue-queued-tech-description-width",
        localised_name = {"rq-settings.queued-descriptions-width"},
        localised_description = {"rq-settings.queued-descriptions-width-detailed"},
        setting_type = "runtime-per-user",
        default_value = 265
    },
})